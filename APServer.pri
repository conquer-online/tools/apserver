HEADERS += \
    ../src/inifile.h \
    ../src/Common/arch.h \
    ../src/Common/common.h \
    ../src/Common/endianness.h \
    ../src/Common/env.h \
    ../src/Common/err.h \
    ../src/Common/log.h \
    ../src/Common/types.h

SOURCES += \
    ../src/inifile.cpp \
    ../src/Common/env.cpp \
    ../src/Common/log.cpp

INCLUDEPATH += \
    ../src \
    ../src/Common \
