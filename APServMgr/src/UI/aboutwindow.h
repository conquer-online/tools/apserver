/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#ifndef _AUTO_PATCH_SERVER_MGR_ABOUT_WINDOW_H_
#define _AUTO_PATCH_SERVER_MGR_ABOUT_WINDOW_H_

#include "common.h"
#include "ui_aboutwindow.h"

/**
 * Dialog shown to specify the information of the application.
 */
class AboutWindow : public QDialog, private Ui::AboutWindow
{
    Q_OBJECT
public:
    /**
     * Create a new AboutWindow.
     *
     * @param[in]   aParent   the parent widget
     */
    explicit AboutWindow(QWidget* aParent = NULL);

protected:
    /**
     * Called when a change event is received.
     *
     * @param[in]   e    the event
     */
    virtual void changeEvent(QEvent* e);

private:
    QGraphicsScene mSceneIcon; //!< graphic scene for the icon
};

#endif // _AUTO_PATCH_SERVER_MGR_ABOUT_WINDOW_H_
