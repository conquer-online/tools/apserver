/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#include "prefwindow.h"

using namespace std;

PrefWindow :: PrefWindow(string& aHost, string& aPath,
                         QWidget* aParent)
    : QDialog(aParent, Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint),
      mHost(aHost), mPath(aPath)
{
    setupUi(this);

    // connect signals & slots
    connect(mBtnApply, SIGNAL(clicked()), SLOT(onApply_triggered()));
    connect(mBtnCancel, SIGNAL(clicked()), SLOT(close()));
    connect(mBtnOk, SIGNAL(clicked()), SLOT(onOk_triggered()));

    mBtnApply->hide(); // HACK !

    mTxtHost->setText(aHost.c_str());
    mTxtPath->setText(aPath.c_str());
}

PrefWindow :: ~PrefWindow()
{

}

void
PrefWindow :: changeEvent(QEvent* e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}

void
PrefWindow :: onApply_triggered()
{
    if (mTxtHost->text().size() > 0 && mTxtPath->text().size() > 0)
    {
        mHost = mTxtHost->text().toStdString();
        mPath = mTxtPath->text().toStdString();
    }
}

void
PrefWindow :: onOk_triggered()
{
    if (mTxtHost->text().size() > 0 && mTxtPath->text().size() > 0)
    {
        mHost = mTxtHost->text().toStdString();
        mPath = mTxtPath->text().toStdString();
        close();
    }
}
