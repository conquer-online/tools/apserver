/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#ifndef _AUTO_PATCH_SERVER_MGR_MAIN_WINDOW_H_
#define _AUTO_PATCH_SERVER_MGR_MAIN_WINDOW_H_

#include "common.h"
#include "ui_mainwindow.h"
#include <string>
#include <map>
#include <QFuture>

#ifdef _WIN32
typedef DWORD pid_t;
#endif // _WIN32

class QIntValidator;

/**
 * Main window of the application.
 */
class MainWindow : public QMainWindow, private Ui::MainWindow
{
public:
    /** The path of the server settings file. */
    static const char SERVER_SETTINGS_PATH[];
    /** The path of the patch list file. */
    static const char PATCH_LIST_PATH[];

    Q_OBJECT
public:
    /**
     * Create a new MainWindow.
     *
     * @param[in]   aParent   the parent widget
     */
    explicit MainWindow(QWidget* aParent = nullptr);

    /* destructor */
    ~MainWindow();

protected:
    /**
     * Called when a change event is received.
     *
     * @param[in]   e    the event
     */
    virtual void changeEvent(QEvent* e);

private:
    /**
     * Load the server settings file.
     *
     * @retval ERROR_SUCCESS on success
     * @return An error code otherwise
     */
    err_t loadServerSettings();

    /**
     * Load the patch list file.
     *
     * @retval ERROR_SUCCESS on success
     * @return An error code otherwise
     */
    err_t loadPatchList();

    /**
     * Refresh the list of patches.
     */
    void refreshList();

private slots:
    /** Action to do when the selection is changed. */
    void onSelection_changed(int aRow);

    /** Action to do when launch is triggered. */
    void onLaunch_triggered();
    /** Action to do when stop is triggered. */
    void onStop_triggered();

    /** Action to do when add is triggered. */
    void onAdd_triggered();
    /** Action to do when edit is triggered. */
    void onEdit_triggered();
    /** Action to do when delete is triggered. */
    void onDelete_triggered();
    /** Action to do when search is triggered. */
    void onSearch_triggered();
    /** Action to do when save is triggered. */
    void onSave_triggered();

    /** Action to do when preferences is triggered. */
    void onPreferences_triggered();

    /** Action to do when minimize is triggered. */
    void onMinimize_triggered();

    /** Action to do when about is triggered. */
    void onAbout_triggered();
    /** Action to do when about Qt is triggered. */
    void onAboutQt_triggered();

private:
    /** Worker checking the deamon status. */
    static void deamonStatusChecker(QLabel* aLabel, bool* aIsRunning, pid_t* aPID,
                                    const bool* aTerminate);

private:
    QIntValidator* mVersionValidator; //!< the version validator

    std::string mHost; //!< the IP address of the files host
    std::string mPath; //!< the path of the patches on the host

    unsigned long mVersion; //!< the current version
    std::map<unsigned long, std::string> mList; //!< the list of all patches

    bool mIsRunning; //!< determine whether or not the deamon is running
    pid_t mPID; //!< the pid of the deamon
    QFuture<void> mWorker; //!< the future of the deamon status checker
    bool mTerminate; //!< determine whther or not the worker must terminate
};

#endif // _AUTO_PATCH_SERVER_MGR_MAIN_WINDOW_H_
