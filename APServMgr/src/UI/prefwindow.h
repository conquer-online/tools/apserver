/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#ifndef _AUTO_PATCH_SERVER_MGR_PREF_WINDOW_H_
#define _AUTO_PATCH_SERVER_MGR_PREF_WINDOW_H_

#include "common.h"
#include "ui_prefwindow.h"
#include <string>

/**
 * Dialog shown to specify the preferences of the application.
 */
class PrefWindow : public QDialog, private Ui::PrefWindow
{
    Q_OBJECT
public:
    /**
     * Create a new PrefWindow.
     *
     * @param[in]   aHost     a reference to the host setting
     * @param[in]   aPath     a reference to the path setting
     * @param[in]   aParent   the parent widget
     */
    explicit PrefWindow(std::string& aHost, std::string& aPath,
                        QWidget* aParent = nullptr);

    /* destructor */
    ~PrefWindow();

protected:
    /**
     * Called when a change event is received.
     *
     * @param[in]   e    the event
     */
    virtual void changeEvent(QEvent* e);

private slots:
    /** Action to do when apply is triggered. */
    void onApply_triggered();
    /** Action to do when ok is triggered. */
    void onOk_triggered();

private:
    std::string& mHost; //!< a reference to the host
    std::string& mPath; //!< a reference to the path
};

#endif // _AUTO_PATCH_SERVER_MGR_PREF_WINDOW_H_
