/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#include "log.h"
#include "mainwindow.h"
#include "aboutwindow.h"
#include "prefwindow.h"
#include "inifile.h"
#include <vector>
#include <fstream>
#include <QMessageBox>
#include <QValidator>
#include <QProcess>
#include <QtConcurrentRun>

#ifdef _WIN32
#include <windows.h> // TerminateProcess, ...
#include <psapi.h> // EnumProcesses...

#pragma comment(lib, "psapi.lib")
#else
#include <signal.h> // kill
#endif // _WIN32

#ifdef __APPLE__
#include <sys/sysctl.h>

// Returns a list of all BSD processes on the system.
// On success, the function returns 0.
// On error, the function returns a BSD errno value.
static int GetBSDProcessList(kinfo_proc** procList, size_t* procCount)
{
    assert(procList != NULL);
    assert(procCount != NULL);

    // example from http://developer.apple.com/qa/qa2001/qa1123.html
    int err = 0;
    kinfo_proc* result = nullptr;
    bool done = false;
    static const int name[] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };

    // Declaring name as const requires us to cast it when passing it to
    // sysctl because the prototype doesn't include the const modifier.
    size_t length = 0;

    *procCount = 0;

    // We start by calling sysctl with result == NULL and length == 0.
    // That will succeed, and set length to the appropriate length.
    // We then allocate a buffer of that size and call sysctl again
    // with that buffer.  If that succeeds, we're done.  If that fails
    // with ENOMEM, we have to throw away our buffer and loop.  Note
    // that the loop causes use to call sysctl with NULL again; this
    // is necessary because the ENOMEM failure case sets length to
    // the amount of data returned, not the amount of data that
    // could have been returned.

    do
    {
        assert(result == nullptr);

        // Call sysctl with a NULL buffer.
        length = 0;
        err = sysctl((int*)name, (sizeof(name) / sizeof(*name)) - 1, nullptr, &length, nullptr, 0);

        if (err == -1)
            err = errno;

        // Allocate an appropriately sized buffer based on the results from the previous call.
        if (err == 0)
        {
            result = (kinfo_proc*)malloc(length);
            if (result == nullptr)
                err = ENOMEM;
        }

        // Call sysctl again with the new buffer.  If we get an ENOMEM
        // error, toss away our buffer and start again.
        if (err == 0)
        {
            err = sysctl((int*)name, (sizeof(name) / sizeof(*name)) - 1, result, &length, nullptr, 0);

            if (err == -1)
                err = errno;

            if (err == 0)
            {
                done = true;
            }
            else if (err == ENOMEM)
            {
                assert(result != nullptr);

                free(result);
                result = nullptr;
                err = 0;
            }
        }
    }
    while (err == 0 && !done);

    // Clean up and establish post conditions.
    if (err != 0 && result != nullptr)
    {
        free(result);
        result = nullptr;
    }

    *procList = result;
    if (err == 0)
        *procCount = length / sizeof(kinfo_proc);

    assert((err == 0) == (*procList != nullptr));
    return err;
}
#endif // __APPLE__

#ifdef __gnu_linux__
#include <dirent.h>
#include <sys/stat.h>
#include <fnmatch.h>

int proc_filter(const struct dirent* aDir)
{
     uid_t user = getuid();
     struct stat info;

     size_t len = strlen(aDir->d_name) + 7;
     char path[len];

     strncpy(path, "/proc/", len);
     strncat(path, aDir->d_name, len);

     if (stat(path, &info) < 0)
     {
         perror("processdir() ==> stat()");
         exit(EXIT_FAILURE);
     }

     return !fnmatch("[1-9]*", aDir->d_name, 0); // && user == info.st_uid;
}
#endif // __gnu_linux__

using namespace std;

/* static */
const char MainWindow::SERVER_SETTINGS_PATH[] = "./settings.cfg";
const char MainWindow::PATCH_LIST_PATH[] = "./patches.lst";

MainWindow::MainWindow(QWidget* aParent)
    : QMainWindow(aParent, Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint),
      mVersionValidator(nullptr), mVersion(0),
      mIsRunning(false), mPID((pid_t)-1), mTerminate(false)
{
    setupUi(this);

    // connect signals & slots
    connect(mLstPatches, SIGNAL(currentRowChanged(int)), SLOT(onSelection_changed(int)));

    connect(mBtnLaunch, SIGNAL(clicked()), SLOT(onLaunch_triggered()));
    connect(mBtnStop, SIGNAL(clicked()), SLOT(onStop_triggered()));
    connect(mBtnAdd, SIGNAL(clicked()), SLOT(onAdd_triggered()));
    connect(mBtnEdit, SIGNAL(clicked()), SLOT(onEdit_triggered()));
    connect(mBtnDelete, SIGNAL(clicked()), SLOT(onDelete_triggered()));
    connect(mBtnSearch, SIGNAL(clicked()), SLOT(onSearch_triggered()));
    connect(mBtnSave, SIGNAL(clicked()), SLOT(onSave_triggered()));

    connect(mMniPreferences, SIGNAL(triggered()), SLOT(onPreferences_triggered()));

    connect(mMniMinimize, SIGNAL(triggered()), SLOT(onMinimize_triggered()));

    connect(mMniAbout, SIGNAL(triggered()), SLOT(onAbout_triggered()));
    connect(mMniAboutQt, SIGNAL(triggered()), SLOT(onAboutQt_triggered()));

    // set the deamon status text...
    mWorker = QtConcurrent::run(&MainWindow::deamonStatusChecker,
                                mTxtStatus, &mIsRunning, &mPID, &mTerminate);

    err_t err = ERROR_SUCCESS;

    // init the logger...
    DOIF(err, Logger::init("./", "apservmgr.log"));

    // parse the config files...
    DOIF(err, loadServerSettings());
    DOIF(err, loadPatchList());

    if (!IS_SUCCESS(err))
    {
        QMessageBox::critical(this, tr("Fatal error !"),
                              tr("Failed to parse the configuration files."));
        abort();
    }

    mVersionValidator = new QIntValidator(0, 999999, this);
    mTxtAddVersion->setValidator(mVersionValidator);
    mTxtEditVersion->setValidator(mVersionValidator);
    mTxtSearchVersion->setValidator(mVersionValidator);
    mTxtCurVersion->setValidator(mVersionValidator);

    mTxtCurVersion->setText(QString::number(mVersion));
    refreshList();
}

MainWindow :: ~MainWindow()
{
    mTerminate = true;
    mWorker.waitForFinished();

    SAFE_DELETE(mVersionValidator);
}

void
MainWindow :: changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}

err_t
MainWindow :: loadServerSettings()
{
    err_t err = ERROR_SUCCESS;

    IniFile settings;
    DOIF(err, settings.open(MainWindow::SERVER_SETTINGS_PATH));

    if (IS_SUCCESS(err))
    {
        mHost = settings.readString("APSERVD/HOST", "127.0.0.1");
        mPath = settings.readString("APSERVD/PATH", "/");

        LOG(INFO, "The host is %s and the absolute path is %s.",
            mHost.c_str(), mPath.c_str());
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            MainWindow::SERVER_SETTINGS_PATH);
        err = ERROR_OPEN_FAILED;
    }

    return err;
}

err_t
MainWindow :: loadPatchList()
{
    err_t err = ERROR_SUCCESS;

    IniFile list;
    DOIF(err, list.open(MainWindow::PATCH_LIST_PATH));

    if (IS_SUCCESS(err))
    {
        vector<string> sections;
        list.getSections(sections);

        mVersion = list.readUInt32("APSERVD/VERSION", 1);
        LOG(INFO, "Current version is %lu.", mVersion);

        for (vector<string>::const_iterator
                it = sections.begin(), end = sections.end();
             ERROR_SUCCESS == err && it != end; ++it)
        {
            const string& section = *it;
            if (section != "APSERVD")
            {
                unsigned long version = list.readUInt32(section + "/VERSION", 0);
                string patch = list.readString(section + "/PATCH", "FAIL");

                if (version != 0 && patch != "FAIL")
                {
                    if (mList.find(version) == mList.end())
                    {
                        LOG(DBG, "Found patch %s for version %lu.",
                            patch.c_str(), version);
                        mList[version] = patch;
                    }
                    else
                    {
                        LOG(WARN, "Duplicated entry for version %lu !",
                            version);
                    }
                }
                else
                {
                    LOG(ERROR, "Failed to get the version and/or the patch for the section %s.",
                        section.c_str());
                }
            }
        }
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            MainWindow::PATCH_LIST_PATH);
        err = ERROR_OPEN_FAILED;
    }

    return err;
}

void
MainWindow :: refreshList()
{
    int row = 0;
    mLstPatches->clear();
    for (map<unsigned long, string>::const_iterator
            it = mList.begin(), end = mList.end();
         it != end; ++it)
    {
        char buf[16];
        snprintf(buf, sizeof(buf), "%lu", it->first);

        mLstPatches->insertItem(row++, buf);
    }
}

void
MainWindow :: onSelection_changed(int aRow)
{
    if (aRow >= 0)
    {
        QListWidgetItem* item = mLstPatches->item(aRow);
        unsigned long version = item->text().toULong();

        map<unsigned long, string>::const_iterator it = mList.find(version);
        if (it != mList.end())
        {
            mTxtEditVersion->setText(QString::number(it->first));
            mTxtEditPatch->setText(QString(it->second.c_str()));
        }
    }
}

void
MainWindow :: onLaunch_triggered()
{
    if (!mIsRunning && mPID == (pid_t)-1)
    {
        QProcess deamon;
        deamon.startDetached("./apservd", QStringList(), "./");
    }
}

void
MainWindow :: onStop_triggered()
{
    if (mIsRunning && mPID != (pid_t)-1)
    {
        #ifdef _WIN32
        HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, mPID);
        if (hProcess == NULL)
            return;

        TerminateProcess(hProcess, 0);

        CloseHandle(hProcess);
        #else
        kill(mPID, SIGTERM);
        #endif // _WIN32
    }
}

void
MainWindow :: onAdd_triggered()
{
    if (mTxtAddVersion->text().size() > 0 && mTxtAddPatch->text().size() > 0)
    {
        unsigned long version = mTxtAddVersion->text().toULong();
        map<unsigned long, string>::iterator it = mList.find(version);

        if (it == mList.end())
        {
            LOG(INFO, "Added patch %s for version %lu.",
                mTxtAddPatch->text().toStdString().c_str(), version);

            mList[version] = mTxtAddPatch->text().toStdString();
            refreshList();
        }
        else
        {
            QMessageBox::warning(this, tr("Duplicated entry !"),
                                 tr("You are trying to add an entry, but the version is already there. Please edit the entry."));
        }
    }
}

void
MainWindow :: onEdit_triggered()
{
    if (mTxtEditVersion->text().size() > 0 && mTxtEditPatch->text().size() > 0)
    {
        unsigned long version = mTxtEditVersion->text().toULong();
        map<unsigned long, string>::iterator it = mList.find(version);

        if (it != mList.end())
        {
            LOG(INFO, "Updated the patch of the version %lu to %s.",
                version, mTxtEditPatch->text().toStdString().c_str());
            it->second = mTxtEditPatch->text().toStdString();
        }
    }
}

void
MainWindow :: onDelete_triggered()
{
    if (mTxtEditVersion->text().size() > 0)
    {
        unsigned long version = mTxtEditVersion->text().toULong();
        map<unsigned long, string>::iterator it = mList.find(version);

        if (it != mList.end())
        {
            LOG(INFO, "Removed the patch of the version %lu.",
                version);
            mList.erase(it);
            refreshList();
        }
    }
}

void
MainWindow :: onSearch_triggered()
{
    if (mTxtSearchVersion->text().size() > 0)
    {
        unsigned long version = mTxtSearchVersion->text().toULong();
        map<unsigned long, string>::const_iterator it = mList.find(version);

        if (it != mList.end())
        {
            mTxtEditVersion->setText(QString::number(it->first));
            mTxtEditPatch->setText(QString(it->second.c_str()));
        }
    }
}

void
MainWindow :: onSave_triggered()
{
    ofstream stream(MainWindow::PATCH_LIST_PATH);
    if (stream.good())
    {
        stream << "[APSERVD]" << endl;
        stream << "VERSION = " << mTxtCurVersion->text().toUInt() << endl;
        stream << endl;

        for (map<unsigned long, string>::const_iterator
                it = mList.begin(), end = mList.end();
             it != end; ++it)
        {
            stream << "[" << it->first << "]" << endl;
            stream << "VERSION = " << it->first << endl;
            stream << "PATCH = " << it->second << endl;
            stream << endl;
        }

        stream.close();

        QMessageBox::information(this, tr("Saved !"),
                                 tr("The updates have been saved to the file. Please restart the deamon."));
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            MainWindow::PATCH_LIST_PATH);

        QMessageBox::critical(this, tr("Fatal error !"),
                              tr("Failed to open the configuration files."));
        abort();
    }
}

void
MainWindow :: onPreferences_triggered()
{
    PrefWindow dlg(mHost, mPath, this);
    dlg.exec();


    err_t err = ERROR_SUCCESS;

    IniFile settings;
    DOIF(err, settings.open(MainWindow::SERVER_SETTINGS_PATH));

    if (IS_SUCCESS(err))
    {
        DOIF(err, settings.writeValue("APSERVD/HOST", mHost.c_str()));
        DOIF(err, settings.writeValue("APSERVD/PATH", mPath.c_str()));

        LOG(INFO, "The host is now %s and the absolute path is now %s.",
            mHost.c_str(), mPath.c_str());
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            MainWindow::SERVER_SETTINGS_PATH);

        QMessageBox::critical(this, tr("Fatal error !"),
                              tr("Failed to open the configuration files."));
        abort();
    }
}

void
MainWindow :: onMinimize_triggered()
{
    setWindowState(Qt::WindowMinimized);
}

void
MainWindow :: onAbout_triggered()
{
    AboutWindow dlg(this);
    dlg.exec();
}

void
MainWindow :: onAboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}

/* static */
void
MainWindow :: deamonStatusChecker(QLabel* aLabel, bool* aIsRunning, pid_t* aPID,
                                  const bool* aTerminate)
{
    ASSERT(aLabel != nullptr);
    ASSERT(aIsRunning != nullptr);
    ASSERT(aPID != nullptr);
    ASSERT(aTerminate != nullptr);

    #ifdef _WIN32
    static const char DEAMON_NAME[] = "apservd.exe";
    #else
    static const char DEAMON_NAME[] = "apservd";
    #endif // _WIN32i

    const QString format = aLabel->text();
    size_t count = 0;

    #ifdef __APPLE__
    kinfo_proc* result = new kinfo_proc();
    #endif // __APPLE__

    while (!(*aTerminate))
    {
        count = 0;
        *aPID = -1;
        *aIsRunning = false;

        #ifdef __APPLE__
        if (GetBSDProcessList(&result, &count) == 0)
        {
            for (size_t i = 0; i < count; ++i)
            {
                kinfo_proc* proc = nullptr;
                proc = &result[i];

                if (strcmp(DEAMON_NAME, proc->kp_proc.p_comm) == 0)
                {
                    *aPID = proc->kp_proc.p_pid;
                    *aIsRunning = true;
                    break;
                }
            }
        }
        #elif defined(_WIN32)
        DWORD processes[1024], len = 0;
        if (EnumProcesses(processes, sizeof(processes), &len) != 0)
        {
            count = len / sizeof(DWORD);

            for (size_t i = 0; i < count; ++i)
            {
                if (processes[i] != 0)
                {
                    char processName[MAX_PATH] = "\0";
                    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                                  FALSE, processes[i]);

                    if (hProcess != NULL)
                    {
                        HMODULE hMod;
                        if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &len) != 0)
                        {
                            GetModuleBaseNameA(hProcess, hMod,
                                               processName, sizeof(processName));

                            if (strcmp(DEAMON_NAME, processName) == 0)
                            {
                                *aPID = processes[i];
                                *aIsRunning = true;
                            }
                        }
                    }

                    CloseHandle(hProcess);
                }
            }
        }
        #elif defined(__gnu_linux__)
        struct dirent** processes = nullptr;
        int count = scandir("/proc", &processes, proc_filter, 0);

        if (count >= 0)
        {
            char processName[1024];
            char buf[1024];

            for (int i = 0; i < count; ++i)
            {
                processName[0] = '\0'; buf[0] = '\0';
                snprintf(processName, sizeof(processName), "/proc/%s/cmdline", processes[i]->d_name);

                FILE* file = fopen(processName, "rb");
                if (file != nullptr)
                {
                    size_t size = fread(processName, sizeof(char), sizeof(processName), file);
                    if (size > 0 && processName[size - 1] == '\n')
                        processName[size - 1] = '\0';

                    size_t startIndex = 0;
                    for (size_t i = 0, len = strlen(processName); i < len; ++i)
                    {
                        if (processName[i] == '/')
                            startIndex = i;
                    }

                    if (processName[startIndex] == '/')
                        ++startIndex;

                    strncpy(buf, &processName[startIndex], sizeof(buf));
                    strncpy(processName, buf, sizeof(processName));

                    fclose(file);
                }

                if (strcmp(DEAMON_NAME, processName) == 0)
                {
                    pid_t pid = -1;
                    sscanf(processes[i]->d_name, "%d", &pid);

                    *aPID = pid;
                    *aIsRunning = true;
                }

                free(processes[i]);
            }

            free(processes);
        }
        else
            perror("Not enough memory.");

        #else
        #error "No support for listing running processes."
        #endif // __APPLE__ | _WIN32 | Linux | Others

        QString text = QString().sprintf(qPrintable(format),
                                         *aIsRunning ? tr("#11952a").toStdString().c_str() : tr("#b50e0e").toStdString().c_str(),
                                         *aIsRunning ? tr("RUNNING").toStdString().c_str() : tr("STOPPED").toStdString().c_str());
        aLabel->setText(text);

        for (size_t i = 0; !(*aTerminate) && i < 10; ++i)
        {
            mssleep(500);
        }
    }

    #ifdef __APPLE__
    SAFE_DELETE(result);
    #endif // __APPLE__
}
