/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#include "aboutwindow.h"

AboutWindow :: AboutWindow(QWidget* aParent)
    : QDialog(aParent, Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint)
{
    setupUi(this);

    // set the icon...
    mSceneIcon.addPixmap(QPixmap(":/Icon.Image"));
    mImgIcon->setScene(&mSceneIcon);


    QString version = QString().sprintf(qPrintable(mLblVersion->text()),
                                      0, 1, 1, TARGET_ARCH);
    mLblVersion->setText(version);

    QString copyright = QString().sprintf(qPrintable(mLblCopyright->text()),
                                          2014, 2014, "CptSky");
    mLblCopyright->setText(copyright);
}

void
AboutWindow :: changeEvent(QEvent* e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}
