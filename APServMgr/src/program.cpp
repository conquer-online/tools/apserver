/*
 * ****** AutoPatch Server Manager - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#include <QApplication>
#include <QTranslator>
#include <QDir>

#include "mainwindow.h"

#include <stdlib.h> // srandom()
#include <time.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    srand(time(NULL));

    #ifdef __APPLE__
    QDir::setCurrent(QCoreApplication::applicationDirPath());
    #endif // __APPLE__

    #ifdef __APPLE__
    QTranslator translator;
    QString locale = QLocale::system().name().section('_', 0, 0);

    translator.load(QCoreApplication::applicationDirPath() + "/../Resources/" + locale + ".qm");
    app.installTranslator(&translator);
    #endif // __APPLE__

    MainWindow dlg;
    dlg.show();

    return app.exec();
}
