QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

# Check Qt version
QT_VERSION = $$[QT_VERSION]
QT_VERSION = $$split(QT_VERSION, ".")
QT_VER_MAJ = $$member(QT_VERSION, 0)
QT_VER_MIN = $$member(QT_VERSION, 1)

TARGET = APServMgr
TEMPLATE = app

macx {
CONFIG += app_bundle
CONFIG += x86 x86_64 ppc ppc64
}

include(../APServer.pri)

OBJECTS_DIR = tmp
MOC_DIR = tmp
RCC_DIR = tmp
UI_DIR = tmp

HEADERS += \
    src/UI/aboutwindow.h \
    src/UI/mainwindow.h \
    src/UI/prefwindow.h

SOURCES += \
    src/program.cpp \
    src/UI/aboutwindow.cpp \
    src/UI/mainwindow.cpp \
    src/UI/prefwindow.cpp

FORMS += \
    src/UI/aboutwindow.ui \
    src/UI/mainwindow.ui \
    src/UI/prefwindow.ui

RESOURCES += \
    res/mainwindow.qrc

TRANSLATIONS += \
    locales/fr.ts

INCLUDEPATH += \
    src \
    src/UI

OTHER_FILES += \
    data/settings.cfg \
    data/patches.lst

# WIN32 stuff
win32 {
OTHER_FILES += \
    res/win32.rc

RC_FILE += \
    res/win32.rc

QMAKE_CFLAGS += -D_CRT_SECURE_NO_WARNINGS
QMAKE_CXXFLAGS += -D_CRT_SECURE_NO_WARNINGS
}

# Mac OS X stuff
macx {
OTHER_FILES += \
    res/darwin_10.3.9.plist \
    res/darwin_10.5.8.plist \
    res/darwin_10.6.8.plist

ICON = res/icon.icns

# Panthers - Leopard (PPC)
if(equals(QT_VER_MAJ, 4)) {
   if(lessThan(QT_VER_MIN, 7)) {
      QMAKE_INFO_PLIST = res/darwin_10.3.9.plist
      QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.3.9
   }
}

# Leopard - Mountain Lion
if(equals(QT_VER_MAJ, 4)) {
   if(greaterThan(QT_VER_MIN, 7)) {
      QMAKE_INFO_PLIST = res/darwin_10.5.8.plist
      QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5.8
   }
}

# Snow Leopard - ???
if(equals(QT_VER_MAJ, 5)) {
    QMAKE_INFO_PLIST = res/darwin_10.6.8.plist
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.6.8
}


# START: qmake 4.8 workaround
QMAKE_INFO_PLIST_OUT = $${TARGET}.app/Contents/Info.plist

missing.target = dummy
missing.depends = $${TARGET}.app/Contents/Info.plist $${TARGET}.app/Contents/Resources/icon.icns

QMAKE_EXTRA_TARGETS += missing
QMAKE_PRE_LINK += make dummy
# END
}

# UNIX stuff...
unix:!macx {

}

# UNIX-like stuff...
unix {
QMAKE_CFLAGS += -Wextra
QMAKE_CXXFLAGS += -Wextra
}

# copying data to build directory...
win32 {
    WIN_PWD = $${replace(PWD, /, \\)}
    WIN_OUT_PWD = $${replace(OUT_PWD, /, \\)}

    QMAKE_POST_LINK += "xcopy /e /y \"$${WIN_PWD}\\data\" \"$${WIN_OUT_PWD}\\\""
}

macx {
    QMAKE_POST_LINK += "cp -r \"$${PWD}/data/\" \"$${OUT_PWD}/$${TARGET}.app/Contents/MacOS/\"; " \
                       "cp \"$${OUT_PWD}/../apservd.tmp\" \"$${OUT_PWD}/$${TARGET}.app/Contents/MacOS/apservd\"; " \
                       "cp $${PWD}/locales/*.qm \"$${OUT_PWD}/$${TARGET}.app/Contents/Resources/\""
}

unix:!macx {
    QMAKE_POST_LINK += "cp -r \"$${PWD}/data/.\" \"$${OUT_PWD}\""
}
