<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../src/UI/aboutwindow.ui" line="29"/>
        <source>About APServMgr</source>
        <translation>À propos de APServMgr</translation>
    </message>
    <message>
        <location filename="../src/UI/aboutwindow.ui" line="62"/>
        <source>Version %d.%d.%d (%s)</source>
        <translation>Version %d.%d.%d (%s)</translation>
    </message>
    <message>
        <location filename="../src/UI/aboutwindow.ui" line="93"/>
        <source>Copyright (C) %d - %d %s</source>
        <translation>Copyright (C) %d - %d %s</translation>
    </message>
    <message>
        <location filename="../src/UI/aboutwindow.ui" line="168"/>
        <source>AutoPatch Server Manager</source>
        <translation>AutoPatch Server Manager</translation>
    </message>
    <message>
        <location filename="../src/UI/aboutwindow.ui" line="187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The icon has been designed by &lt;a href=&quot;www.dellustrations.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Wendell Fernandes&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;L&apos;icône a été créé par &lt;a href=&quot;www.dellustrations.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Wendell Fernandes&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>LicenseWindow</name>
    <message>
        <location filename="../src/UI/licensewindow.ui" line="26"/>
        <source>Registration</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.ui" line="44"/>
        <source>Name :</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.ui" line="76"/>
        <source>Serial :</source>
        <translation>N° série :</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.ui" line="102"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.ui" line="115"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="30"/>
        <location filename="../src/UI/licensewindow.cpp" line="99"/>
        <source>Invalid license !</source>
        <translation>License invalide !</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="30"/>
        <source>Found a license, but the it was rejected.</source>
        <translation>Une license a été trouvée, mais elle a été rejetée.</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="58"/>
        <source>Warning !</source>
        <translation>Attention !</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="58"/>
        <source>The program is not registered ! Are you sure to want to quit this dialog ?</source>
        <translation>Le program n&apos;est pas enregistré ! Êtes-vous sûre de vouloir quitter ce dialogue ?</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="92"/>
        <source>Thank you !</source>
        <translation>Merci !</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="92"/>
        <source>The program is now registered !</source>
        <translation>Le programme est maintenant enregistré !</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="96"/>
        <source>Error !</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="96"/>
        <source>Failed to write the license file.</source>
        <translation>Échec de l&apos;écriture du fichier de license.</translation>
    </message>
    <message>
        <location filename="../src/UI/licensewindow.cpp" line="99"/>
        <source>Sorry, the license was rejected.</source>
        <translation>Désolé, la license a été rejetée.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="26"/>
        <source>AutoPatch Server Manager</source>
        <translation>AutoPatch Server Manager</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="62"/>
        <source>Edit Patch</source>
        <translation>Modification d&apos;une mise à jour</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="74"/>
        <location filename="../src/UI/mainwindow.ui" line="162"/>
        <location filename="../src/UI/mainwindow.ui" line="237"/>
        <source>Version :</source>
        <translation>Version :</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="87"/>
        <location filename="../src/UI/mainwindow.ui" line="175"/>
        <source>Patch :</source>
        <translation>Patch :</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="123"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="136"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="150"/>
        <source>Add Patch</source>
        <translation>Ajout d&apos;une mise à jour</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="211"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="225"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="263"/>
        <source>Go</source>
        <translation>Aller</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="277"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="289"/>
        <source>Current Version :</source>
        <translation>Version courante :</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="315"/>
        <source>Deamon Status :</source>
        <translation>Statut du deamon :</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="328"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:%s;&quot;&gt;%s&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="341"/>
        <source>Launch</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="354"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="368"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="383"/>
        <source>Edition</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="389"/>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="395"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="408"/>
        <source>About APServMgr</source>
        <translation>À propos de APServMgr</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="416"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="424"/>
        <source>Minimize</source>
        <translation>Minimiser</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="427"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="432"/>
        <source>Preferences...</source>
        <translation>Préférences...</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="435"/>
        <source>Ctrl+,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.ui" line="443"/>
        <source>Registration...</source>
        <translation>Enregistrement...</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="175"/>
        <location filename="../src/UI/mainwindow.cpp" line="478"/>
        <location filename="../src/UI/mainwindow.cpp" line="509"/>
        <source>Fatal error !</source>
        <translation>Erreur fatale !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="176"/>
        <source>Failed to parse the configuration files.</source>
        <translation>Échec de la lecture des fichiers de configuration.</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="221"/>
        <source>Invalid license !</source>
        <translation>License invalide !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="221"/>
        <source>Found a license, but the it was rejected.</source>
        <translation>Une license a été trouvée, mais elle a été rejetée.</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="224"/>
        <source>Unregistered !</source>
        <translation>Non enregistrée !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="224"/>
        <source>The application is not registered !</source>
        <translation>L&apos;application n&apos;est pas enregistrée !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="391"/>
        <source>Duplicated entry !</source>
        <translation>Doublon !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="392"/>
        <source>You are trying to add an entry, but the version is already there. Please edit the entry.</source>
        <translation>Vous essayez d&apos;ajouter une entrée, mais cette version est déjà présente. Veuillez modifier l&apos;entrée.</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="470"/>
        <source>Saved !</source>
        <translation>Enregistré !</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="471"/>
        <source>The updates have been saved to the file. Please restart the deamon.</source>
        <translation>Les changements ont été apportés au fichier. Veuillez relancer le deamon.</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="479"/>
        <location filename="../src/UI/mainwindow.cpp" line="510"/>
        <source>Failed to open the configuration files.</source>
        <translation>Échec de l&apos;ouverture des fichiers de configuration.</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="626"/>
        <source>#11952a</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="626"/>
        <source>#b50e0e</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="627"/>
        <source>RUNNING</source>
        <translation>EN FONCTION</translation>
    </message>
    <message>
        <location filename="../src/UI/mainwindow.cpp" line="627"/>
        <source>STOPPED</source>
        <translation>ARRÊTÉ</translation>
    </message>
</context>
<context>
    <name>PrefWindow</name>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="26"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="41"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The host is the IP address of the web server (HTTP) hosting the patch files.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;L&apos;hôte est l&apos;adresse IP du serveur web (HTTP) qui héberge les fichiers de mise à jour.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="47"/>
        <source>Host :</source>
        <translation>Hôte :</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="60"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The relative path corresponds to the full path of the folder containing the patches on the host, relative to the root.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le chemin relatif correspond au chemin complet du dossier contenant les fichiers de mise à jour sur l&apos;hôte, relativement au dossier racine.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="66"/>
        <source>Relative Path :</source>
        <translation>Chemin relatif :</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="95"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="111"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/UI/prefwindow.ui" line="127"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
</TS>
