# AutoPatch Server

Copyright (C) 2014 CptSky <br />
https://gitlab.com/conquer-online/tools/apserver

## Overview

AutoPatch Server is a server emulating the official AutoPatch server used by TQ games. It is a C++ deamon combined with a powerful UI for managing the server.

The emulator is developed in C++ with Qt4 and Qt5.

## Features

APServ is a fully functionnal AutoPatch server.

+ Support any POSIX and/or WinAPI operating system with Qt4 / Qt5
+ Support any architecture (little endian & big endian | 32 bit & 64 bit)
+ Separate worker for logging (see apserv.log)
+ Self destructed environment (useful for singletons)

+ apservd
  - Fully functionnal and independent deamon
    * Small footprint (less than 5 MB in memory RAM)
    * Only two INI files for configuring the server

+ APServMgr
  - UI for maintaining the configuration files
    * Small footprint (less than 15 MB in memory)
    * Status of the deamon (and ability to launch / stop the deamon)

+ On Mac OS X, the whole server is self-contened in the APServMgr app.
+ On Mac OS X, APServMgr is also available in French.
+ On Windows, the whole server must be in the same folder.

Documentation about the server can be generated with dOxygen.

## Supported systems

The emulator has been tested on the following platforms:
- Windows XP Professional (SP2)
  - x86_64
- Mac OS X Leopard (10.5.8)
  - ppc, ppc64
- Mac OS X Mavericks (10.9.2)
  - x86, x86_x64

However, the emulator should work without any modification on any POSIX-compliant system (Mac OS X, *NIX, GNU/Linux, etc) and on any WinAPI system.

N.B. This server uses the Windows API and/or the POSIX API and/or the Qt4 API.
