/*
 * ****** AutoPatch Server - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#ifndef _AUTO_PATCH_SERVER_SERVER_H_
#define _AUTO_PATCH_SERVER_SERVER_H_

#include "common.h"
#include "env.h"
#include "tcpserver.h"
#include <time.h>
#include <string>
#include <map>

class NetworkClient;

/**
 * Global server object. It is a singleton and will be created when
 * getting the instance.
 */
class Server : public Environment::Global
{
    // !!! class is a singleton !!!
    PROHIBIT_COPY(Server);

public:
    /** The server port. */
    static const uint16_t SERVER_PORT = 9528;

    /** The path of the server settings file. */
    static const char SERVER_SETTINGS_PATH[];
    /** The path of the patch list file. */
    static const char PATCH_LIST_PATH[];

public:
    /**
     * Get the Server singleton. If the object does not exist yet,
     * it will be created.
     *
     * @returns A reference to the singleton
     */
    static Server& getInstance();

public:
    /* destructor */
    ~Server();

private:
    /**
     * Function callback to handle new connections to the server.
     *
     * @param aClient[in]   The new network client
     */
    static void connectionHandler(NetworkClient* aClient);

    /**
     * Function callback to handle data received by the server.
     *
     * @param aClient[in]   The client that sent the data
     * @param aBuf[in]      A pointer to the buffer containing the data
     * @param aLen[in]      The length in bytes of the buffer
     */
    static void receiveHandler(NetworkClient* aClient, uint8_t* aBuf, size_t aLen);

    /**
     * Function callback to handle disconnections of a client.
     *
     * @param aClient[in]   The client that will be disconnected
     */
    static void disconnectionHandler(NetworkClient* aClient);

private:
    /* constructor */
    Server();

    /**
     * Load the server settings file.
     *
     * @retval ERROR_SUCCESS on success
     * @return An error code otherwise
     */
    err_t loadServerSettings();

    /**
     * Load the patch list file.
     *
     * @retval ERROR_SUCCESS on success
     * @return An error code otherwise
     */
    err_t loadPatchList();

private:
    static Server* sInstance; //!< static instance of the singleton

private:
    TcpServer mAPServer; //!< TCP/IP server for the AP server

    std::string mHost; //!< the IP address of the files host
    std::string mPath; //!< the path of the patches on the host

    unsigned long mVersion; //!< the current version
    std::map<unsigned long, std::string> mList; //!< the list of all patches
};

#endif // _AUTO_PATCH_SERVER_SERVER_H_
