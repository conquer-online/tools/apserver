/*
 * ****** AutoPatch Server - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#include "log.h"
#include "server.h"
#include "networkclient.h"
#include "inifile.h"
#include <errno.h>

using namespace std;

/* static */
Server* Server::sInstance = nullptr;

const char Server::SERVER_SETTINGS_PATH[] = "./settings.cfg";
const char Server::PATCH_LIST_PATH[] = "./patches.lst";

/* static */
Server&
Server :: getInstance()
{
    static volatile long protect = 0;

    if (sInstance == nullptr)
    {
        if (1 == atomic_inc(&protect))
        {
            // create the instance
            sInstance = new Server();
        }
        else
        {
            while (sInstance == nullptr)
                QThread::yieldCurrentThread();
        }
    }
    return *sInstance;
}

Server :: Server()
    : mVersion(0)
{
    err_t err = ERROR_SUCCESS;

    // init the logger...
    DOIF(err, Logger::init("./", "apserv.log"));

    // parse the config files...
    DOIF(err, loadServerSettings());
    DOIF(err, loadPatchList());

    fprintf(stdout, "\n");

    mAPServer.listen(Server::SERVER_PORT);
    mAPServer.onConnect = &Server::connectionHandler;
    mAPServer.onReceive = &Server::receiveHandler;
    mAPServer.onDisconnect = &Server::disconnectionHandler;
    mAPServer.accept();
    fprintf(stdout, "APServer listening on port %u...\n", Server::SERVER_PORT);

    fprintf(stdout, "Waiting for connections...\n");

    ASSERT(err == ERROR_SUCCESS);
}

Server :: ~Server()
{

}

err_t
Server :: loadServerSettings()
{
    err_t err = ERROR_SUCCESS;

    IniFile settings;
    DOIF(err, settings.open(Server::SERVER_SETTINGS_PATH));

    if (IS_SUCCESS(err))
    {
        mHost = settings.readString("APSERVD/HOST", "127.0.0.1");
        mPath = settings.readString("APSERVD/PATH", "/");

        LOG(INFO, "The host is %s and the absolute path is %s.",
            mHost.c_str(), mPath.c_str());
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            Server::SERVER_SETTINGS_PATH);
        err = ERROR_OPEN_FAILED;
    }

    return err;
}

err_t
Server :: loadPatchList()
{
    err_t err = ERROR_SUCCESS;

    IniFile list;
    DOIF(err, list.open(Server::PATCH_LIST_PATH));

    if (IS_SUCCESS(err))
    {
        vector<string> sections;
        list.getSections(sections);

        mVersion = list.readUInt32("APSERVD/VERSION", 1);
        LOG(INFO, "Current version is %lu.", mVersion);

        for (vector<string>::const_iterator
                it = sections.begin(), end = sections.end();
             ERROR_SUCCESS == err && it != end; ++it)
        {
            const string& section = *it;
            if (section != "APSERVD")
            {
                unsigned long version = list.readUInt32(section + "/VERSION", 0);
                string patch = list.readString(section + "/PATCH", "FAIL");

                if (version != 0 && patch != "FAIL")
                {
                    if (mList.find(version) == mList.end())
                    {
                        LOG(DBG, "Found patch %s for version %lu.",
                            patch.c_str(), version);
                        mList[version] = patch;
                    }
                    else
                    {
                        LOG(WARN, "Duplicated entry for version %lu !",
                            version);
                    }
                }
                else
                {
                    LOG(ERROR, "Failed to get the version and/or the patch for the section %s.",
                        section.c_str());
                }
            }
        }
    }
    else
    {
        LOG(ERROR, "Failed to open the file %s.",
            Server::PATCH_LIST_PATH);
        err = ERROR_OPEN_FAILED;
    }

    return err;
}

/* static */
void
Server :: connectionHandler(NetworkClient* aClient)
{
    LOG(DBG, "Incoming connection... %p\n", aClient);
}

/* static */
void
Server :: receiveHandler(NetworkClient* aClient, uint8_t* aBuf, size_t aLen)
{
    static const char CMD_READY[] = "READY";
    static const char CMD_UPDATE[] = "UPDATE %s %s/%s"; // host, path, file
    static const char CMD_FAIL[] = "FAIL";

    Server& server = Server::getInstance();

    if (aClient != nullptr && aBuf != nullptr)
    {
        char* received = new char[aLen + 1];
        memcpy(received, aBuf, aLen);
        received[aLen] = '\0';

        err_t err = ERROR_SUCCESS;

        char* ptr = &received[0];
        unsigned long version = strtoul(received, &ptr, 10);

        if (0 == version || ULONG_MAX == version) // maybe a failure
        {
            switch (errno)
            {
                case EINVAL:
                case ERANGE:
                    {
                        LOG(WARN, "[%p] Failed to parse the msg. %s", aClient, strerror(errno));
                        err = ERROR_BAD_FORMAT;
                        break;
                    }
            }

            if (*ptr != '\0')
            {
                LOG(WARN, "[%p] Failed to convert the msg to integer.", aClient);
                err = ERROR_BAD_FORMAT;
            }
        }

        if (IS_SUCCESS(err))
        {
            LOG(DBG, "[%p] client=%lu, server=%lu.", aClient, version, server.mVersion);

            if (version < server.mVersion)
            {
                char cmd[1024] = { '\0' };

                map<unsigned long, string>::const_iterator it;
                if ((it = server.mList.find(version)) != server.mList.end())
                {
                    snprintf(cmd, sizeof(cmd), CMD_UPDATE,
                             server.mHost.c_str(), server.mPath.c_str(), it->second.c_str());
                }
                else
                {
                    LOG(WARN, "[%p] Could not find a patch for the version %lu.", aClient, version);
                }

                if (IS_SUCCESS(err))
                {
                    LOG(INFO, "[%p] %s", aClient, cmd);
                    aClient->send(cmd, strlen(cmd));
                }
                else
                {
                    LOG(DBG, "[%p] %s", aClient, CMD_FAIL);
                    aClient->send(CMD_FAIL, strlen(CMD_FAIL));
                }
            }
            else if (version == server.mVersion)
            {
                LOG(DBG, "[%p] %s", aClient, CMD_READY);
                aClient->send(CMD_READY, strlen(CMD_READY));
            }
            else
            {
                LOG(DBG, "[%p] %s", aClient, CMD_FAIL);
                aClient->send(CMD_FAIL, strlen(CMD_FAIL));
            }
        }
        else
        {
            LOG(DBG, "[%p] %s", aClient, CMD_FAIL);
            aClient->send(CMD_FAIL, strlen(CMD_FAIL));
        }

        SAFE_DELETE_ARRAY(received);
    }
}

/* static */
void
Server :: disconnectionHandler(NetworkClient* aClient)
{
    LOG(DBG, "Incoming disconnection... %p\n", aClient);
}
