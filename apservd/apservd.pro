QT += core
QT += network
QT -= gui

# Check Qt version
QT_VERSION = $$[QT_VERSION]
QT_VERSION = $$split(QT_VERSION, ".")
QT_VER_MAJ = $$member(QT_VERSION, 0)
QT_VER_MIN = $$member(QT_VERSION, 1)

TARGET = "apservd"
TEMPLATE = app
CONFIG += console

macx {
CONFIG -= app_bundle
CONFIG += x86 x86_64 ppc ppc64
}

include(../APServer.pri)

OBJECTS_DIR = tmp
MOC_DIR = tmp
RCC_DIR = tmp
UI_DIR = tmp

HEADERS += \
    src/server.h \
    src/Network/Sockets/networkclient.h \
    src/Network/Sockets/tcpserver.h

SOURCES += \
    src/program.cpp \
    src/server.cpp \
    src/Network/Sockets/networkclient.cpp \
    src/Network/Sockets/tcpserver.cpp

INCLUDEPATH += \
    src \
    src/Network/Sockets

OTHER_FILES += \
    data/settings.cfg \
    data/patches.lst

# WIN32 stuff
win32 {
#OTHER_FILES += \
#    res/win32.rc

#RC_FILE += \
#    res/win32.rc

QMAKE_CFLAGS += -D_CRT_SECURE_NO_WARNINGS
QMAKE_CXXFLAGS += -D_CRT_SECURE_NO_WARNINGS
}

# Mac OS X stuff
macx {

}

# UNIX stuff...
unix:!macx {

}

# UNIX-like stuff...
unix {
QMAKE_CFLAGS += -Wextra
QMAKE_CXXFLAGS += -Wextra
}

# copying data to build directory...
win32 {
    WIN_PWD = $${replace(PWD, /, \\)}
    WIN_OUT_PWD = $${replace(OUT_PWD, /, \\)}

    QMAKE_POST_LINK += "xcopy /e /y \"$${WIN_PWD}\\data\" \"$${WIN_OUT_PWD}\\\""
}

macx {
    QMAKE_POST_LINK += "cp -r \"$${PWD}/data/\" \"$${OUT_PWD}/\"; cp \"$${OUT_PWD}/$${TARGET}\" \"$${OUT_PWD}/../$${TARGET}.tmp\""
}

unix:!macx {
    QMAKE_POST_LINK += "cp -r \"$${PWD}/data/.\" \"$${OUT_PWD}\""
}
