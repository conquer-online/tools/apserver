/*
 * ****** AutoPatch Server - Open Source ******
 * Copyright (C) 2014 CptSky
 *
 * Please read the WARNING, DISCLAIMER and PATENTS
 * sections in the LICENSE file.
 */

#ifndef _AUTO_PATCH_SERVER_COMMON_H_
#define _AUTO_PATCH_SERVER_COMMON_H_

#include "arch.h"
#include "types.h"
#include "endianness.h"
#include "err.h"

#ifdef _WIN32
#define NOMINMAX // want std::min() & std::max() defined...
#include <windows.h>

#ifdef _MSC_VER // Visual Studio will complain for linking...
#pragma comment(lib, "WinMM") // for timeGetTime()
#endif

#else
#include <unistd.h> // sysctl, sysconf
#include <sys/time.h> // for timeGetTime()
#endif

#ifdef __APPLE__
#include <libkern/OSAtomic.h>
#endif // atomics

/*
 *****************************************************
 * Safe delete macros
 ****************************************************
 */

// Delete a pointer and set the pointer to NULL.
#define SAFE_DELETE(ptr)                          \
    delete ptr;                                   \
    ptr = nullptr;

// Delete an array and set the pointer to NULL.
#define SAFE_DELETE_ARRAY(ptr)                    \
    delete[] ptr;                                 \
    ptr = nullptr;

/*
 *****************************************************
 * Protection macros
 ****************************************************
 */

// Prohibit the copy of the object
#define PROHIBIT_COPY(__class)                   \
    private:                                        \
        __class(const __class&);                    \
        __class& operator=(const __class&)

/*
 *****************************************************
 * Common macros
 ****************************************************
 */

// Quote strings in macro */
#define STRINGIFY_(str) #str
#define STRINGIFY(str) STRINGIFY_(str)

/*
 *****************************************************
 * Cross-compiling definitions
 ****************************************************
 */

// If using Visual Studio, __FUNCTION__ is not defined, but __func__ is
#ifndef _MSC_VER
#define __FUNCTION__ __func__
#endif // _MSC_VER

// If using Visual Studio, and C++11 is not implemented
// POSIX-compliant platforms define those functions, so C++11 is not required
#if defined(_MSC_VER) && __cplusplus < 201103L
#define snprintf _snprintf
#endif

// If __TIMESTAMP__ is not defined, create it by merging __DATE__ and __TIME__
#ifndef __TIMESTAMP__
#define __TIMESTAMP__ __DATE__" "__TIME__
#endif

#if defined(__APPLE__)
#   if defined(TARGET_INSTR_X86_64) || defined(TARGET_INSTR_PPC64)
#       define atomic_inc(ptr) OSAtomicIncrement64Barrier(((volatile int64_t*)ptr))
#   else
#       define atomic_inc(ptr) OSAtomicIncrement32Barrier(((volatile int32_t*)ptr))
#   endif
#elif defined (_WIN32)
#   define atomic_inc(ptr) InterlockedIncrement((ptr))
#elif defined(__GNUC__)
#   define atomic_inc(ptr) (__sync_fetch_and_add((ptr), 1) + 1)
#else
#   error "Need some more porting work for atomic_inc."
#endif

#ifndef _WIN32
#define mssleep(ms) usleep(ms * 1000)
#else
#define mssleep(ms) Sleep(ms)
#endif // _WIN32

#endif // _AUTO_PATCH_SERVER_COMMON_H_
